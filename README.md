# sipmTrigger

The SiPMTrigger realizes a small-scale design for coincidence readout of SiPMs as a trigger or veto detector. It consists of a custom mixed signal frontend board featuring signal amplification, discrimination and a coincidence unit for rates up to 200 kHz. 

## Getting started

Board:
v1: Use Altium Designer https://www.altium.com/de/altium-designer
v2: Use KiCAD https://www.kicad.org/

Firmware:
Arduino Studio https://www.arduino.cc/en/software

## Contributors

Simon Schmidt, Physikalisches Institut, Heidelberg University  
Jannis Weimar, Physikalisches Institut, Heidelberg University  
Markus Köhli, Physikalisches Institut, Heidelberg University  
Fabian Schmidt, Physikalisches Institut, University of Bonn  
Alexander Lambertz, Physikalisches Institut, University of Bonn  
Laura Weber, Physikalisches Institut, University of Bonn  
Jochen Kaminski, Physikalisches Institut, University of Bonn


## Acknowledgment
We acknowledge the electronics workshop of the Physiklaisches Institut, Heidelberg University, for their steady support and helpful suggestions. We acknowledge Prof. Dr. Ulrich Schmidt, Heidelberg University, for supporting this project. We acknowledge StyX Neutronica for constant support of this project. 

## License
Board files and related documents are licence under CC BY-SA 4.0. Firmware code is licenced under GNU GPLv3.

## Project status
Readme Files to be added.
